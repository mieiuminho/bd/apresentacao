# Migração da base de dados MySQL para MongoDB

* Executar o script _db_export.sql_.
* Correr _Migrations_.
* Correr o script _populate.js_ (`npm run populate`).
