# Views (por exemplo para o _Organizador_)

~~~sql
DROP VIEW IF EXISTS `clinicalTrials`.`viewProvasOrganizador`;
USE `clinicalTrials`;
CREATE OR REPLACE VIEW `viewProvasOrganizador` AS
SELECT o.id                        AS idOrganizador,
       o.nome                      AS nomeOrganizador,
       p.nome                      AS nomeProva,
       m.nome                      AS nomeModalidade,
       COUNT(DISTINCT ap.idAtleta) AS numAtletas
FROM Organizador AS o
        INNER JOIN
    Prova AS p ON o.id = p.idOrganizador
        INNER JOIN
    Modalidade AS m ON p.idModalidade = m.id
        INNER JOIN
    Atleta_In_Prova AS ap ON ap.idProva = p.id
GROUP BY p.id;
~~~

Notes:

    Aqui está patente a view do organizador de provas. Este consegue, para cada
    prova que organiza, ver o identificador do organizador, o nome do
    organizador, o nome da prova, a modalidade e o número de atletas inscritos.
