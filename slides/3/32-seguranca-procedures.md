# _Procedures_ (por exemplo para os _Atletas_)

~~~ mysql
CREATE ROLE Atleta;
GRANT EXECUTE ON PROCEDURE clinicalTrials.checkFutureExams TO ATLETA;
GRANT EXECUTE ON PROCEDURE clinicalTrials.fazerPedidosForProvaHospital TO ATLETA;
GRANT EXECUTE ON PROCEDURE clinicalTrials.getConsultasPorEspecialidade TO ATLETA;
GRANT EXECUTE ON PROCEDURE clinicalTrials.getExamsDone TO ATLETA;
GRANT EXECUTE ON PROCEDURE clinicalTrials.getExamsNeeded TO ATLETA;
GRANT EXECUTE ON PROCEDURE clinicalTrials.getFuturasProvas TO ATLETA;
GRANT EXECUTE ON PROCEDURE clinicalTrials.getOrganizador TO ATLETA;

CREATE USER 'atleta'@'%' IDENTIFIED BY '123456789';
GRANT ATLETA TO 'atleta'@'%';
~~~

Notes:

    A atribuição de _roles_ limita a utilização da base de dados por parte de
    diferentes utilizadores. No caso apresentado, o do _role_ Atleta, este
    apenas pode realizar os procedures cujas permissões lhe são atribuídas.
