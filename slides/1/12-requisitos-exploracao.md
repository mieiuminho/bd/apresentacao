# Requisitos de exploração

<br>
<br>
<br>
<br>

* O atleta deve conseguir agendar testes clínicos.

* Deverá ser possível aceder ao stock de equipamentos médicos num hospital.

* Deverá ser possível saber quais os exames necessários para a inscrição numa
   prova.


Notes:

    Este tipo de requisitos especificam as operações (interrogações) que devem
    ser realizáveis na base de dados. Mais uma vez, encontram-se aqui apenas
    algumas das que reunimos na fase de análise de requisitos.
