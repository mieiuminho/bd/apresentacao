# Requisitos de Descrição

<br>

* Quando um atleta se regista no CCT deve fornecer o seu email, nome, número
   de telemóvel, data de nascimento, altura, peso, modalidade e género.

* Quando um organizador se regista no CCT tem de fornecer o seu nome, email,
   número de telemóvel e o nome da prova que organiza.

* Quando um médico se regista no CCT tem de fornecer o seu email, nome,
   especialidade médica e hospital a que está contratualmente ligado.

* Um atleta pode inscrever-se em diversas provas.

* Cada atleta compete numa só modalidade.

Notes:

    Este tipo de requisitos identificam com mais pormenor os atributos de cada
    entidade.
