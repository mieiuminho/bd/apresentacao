## Espaço ocupado por cada entrada de tabela

| Tabela                  | Tamanho esperado de uma entrada (bytes) |
|-------------------------|-----------------------------------------|
| Medico                  | 268                                     |
| Equipamento_In_Exame    | 8                                       |
| Equipamento             | 50                                      |
| Pedidos_Exame           | 12                                      |
| Hospital                | 49                                      |
| Equipamento_In_Hospital | 12                                      |
