## Espaço ocupado por cada entrada de tabela

| Tabela                  | Tamanho esperado de uma entrada (bytes) |
|-------------------------|-----------------------------------------|
| Atleta                  | 285                                     |
| Modalidade              | 132                                     |
| Atleta_In_Prova         | 8                                       |
| Exame_In_Consulta       | 8                                       |
| Consulta                | 30                                      |
