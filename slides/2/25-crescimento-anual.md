# Crescimento anual estimado da base de dados

* 1000 atletas
* 50 provas
* 2 hospitais
* 1 especialidade médica
* 20 médicos
* 5 organizadores de provas
* 5 equipamentos para realizar exames
* 5 tipos de exames

Desta forma, estimamos um aumento do espaço ocupado pela base de dados de
0.448245MegaBytes por ano.
