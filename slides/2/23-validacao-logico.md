# Validação do modelo lógico

<br>
<br>
<br>
<br>

<img src="images/algebra.png" alt="" style="float:center;display:inline-block;padding=100px">

Notes:

    Na validação do modelo lógico utilizamos álgebra relacional para verificar
    a viablidade das operações que havíamos estabelecido perante o modelo
    lógico a que chegamos. Como conseguimos estabelecer todas as operações,
    validamos o modelo.
