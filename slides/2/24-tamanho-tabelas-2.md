## Espaço ocupado por cada entrada de tabela

| Tabela                  | Tamanho esperado de uma entrada (bytes) |
|-------------------------|-----------------------------------------|
| Organizador             | 177                                     |
| Prova                   | 156                                     |
| ExamesNeededInProva     | 16                                      |
| Exame                   | 56                                      |
| Especialidade           | 132                                     |
