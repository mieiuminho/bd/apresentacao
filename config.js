module.exports = {
    title: 'Centralized Clinical Tests',
    description: '',
    course: 'Bases de Dados',
    authors: [
        { name:'Nelson Estevão', number: 'A76434', },
        { name:'Pedro Ribeiro', number: 'A85493', },
        { name:'Rui Mendes', number: 'A83712', },
        { name:'Hugo Carvalho', number: 'A85579'}
    ],
}
